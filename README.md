## Introduction

This is an attempt to build a shopping cart spa with a main category page containing a grid to display the products wherein a user can hover on the product to view the details on the product.

## Table of Contents

- [Requirements](#requirements)
- [Build and run](#build-and-run)
- [Frameworks](#frameworks)
- [What Works And What is Yet To Be Implemented](#what-works-and-what-is-yet-to-be-implemented)

#### Requirements

The project depends on npm which is installed with nodejs. To install nodejs, please follow the instructions on this page: https://docs.npmjs.com/getting-started/installing-node

#### Build and run

Clone the repo and run npm start to bring up the development server.

```
git clone https://prashanth_bg@bitbucket.org/prashanth_bg/shopping-cart.git
```

From the project directory, run
```
npm install
```
Followed by
```
npm start
```
Once the server has started, point the browser to http://localhost:3000/ to view the application.

#### Frameworks

This application uses the following framewoks for building the shopping cart

React JS - Lightweight not much of overhead.
Foundation CSS - Provides some of base components to build higher order components.
Webpack - Build system which works well with React.
Redux - For future state management. Right now there is no state.

#### What Works And What is Yet To Be Implemented

##### What works

Main Category page with Menu, Banner, and the grid.
The grid displays products with associated details
On hover of the grid, the overlay with View Details and Add to Cart component is rendered
On Click of the View Details, the user is directed to the details page.
The details page contains the enlarged view of the image, the price, description.
The gird works when the product data (mock/data.json) is populated with more data.
For simplicity, i have added the id property to the json data to easily identify the product.

##### Yet to be implemented.

Add to cart feature is in progress.
View Cart page is yet to be worked upon.
Cart modal on click of cart in the menu bar is yet to be done.
Tests
Responsive not tested on devices.
