import React from 'react';
import ReactDOM from 'react-dom';
import Main from './components/Main';
import ProductCategoriesPage from './components/ProductCategoriesPage/ProductCategoriesPage';
import ProductDetailsPageContainer from './components/ProductDetailsPage/ProductDetailsPageContainer';
import {Route, BrowserRouter} from 'react-router-dom';
import registerServiceWorker from './registerServiceWorker';
import 'foundation-sites/dist/css/foundation.min.css';
import 'foundation-sites/dist/css/foundation-float.min.css';
import './index.css';

ReactDOM.render(
  <BrowserRouter>
    <Main>
        <Route exact path="/" component={ProductCategoriesPage}></Route>
        <Route path="/details/:id" component={ProductDetailsPageContainer}></Route>
    </Main>
  </BrowserRouter>,
  document.getElementById('root')
);
registerServiceWorker();
