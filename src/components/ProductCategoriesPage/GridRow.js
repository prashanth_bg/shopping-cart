import React from 'react';
import GridColumn from './GridColumn';
import {
  Row
} from 'react-foundation';

class GridRow extends React.Component {
  render () {

    let columns = [],
        size = 2,
        smallSize = 0 ;

    for (let i=0; i < this.props.columns.length; i++) {
      i === 0 ? smallSize = size : smallSize = smallSize * 2;
      columns.push(
        <GridColumn key={i} small={smallSize} large={4}
          id={this.props.columns[i].id}
          image={require('../../images/' + this.props.columns[i].image)}
          title={this.props.columns[i].title}
          brand={this.props.columns[i].brand}
          price={this.props.columns[i].price}>
        </GridColumn>);
    }

    return (
      <Row className="display">
        {columns}
      </Row>
    );
  }
}

export default GridRow;
