import React from 'react';
import {
  MediaObject,
  MediaObjectSection
} from 'react-foundation';
import leftImage from '../../images/plates-header.jpg';

class Banner extends React.Component {
  render () {
    return (
      <MediaObject>
        <MediaObjectSection>
          <div className="category-media-area-image-left">
            <img src={leftImage} alt="plates images"/>
            <div className="card">
              <div className="category-media-text-container">
                <div className="category-media-area-heading">
                  Plates
                </div>
                <div className="category-media-area-text">
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Nam hendrerit nisi sed sollicitudin pellentesque.
                </div>
              </div>
            </div>
          </div>
        </MediaObjectSection>
      </MediaObject>
    );
  }
}

export default Banner;
