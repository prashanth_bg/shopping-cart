import React from 'react';
import GridRow from './GridRow';

class ProductGrid extends React.Component {
  render () {

    let chunks = [],
        rows = [],
        id = 0;

    for (let i = 0; i <= this.props.products.length; i++) {
      if((i > 0) && (i % 3 === 0)) {
        let chunkProps = chunks.slice();
        rows.push(<GridRow key={id} columns={chunkProps}></GridRow>)
        id = id + 1;
        chunks.length = 0;
        chunks.push(this.props.products[i]);
      } else {
        chunks.push(this.props.products[i]);
      }
    }

    return (
      <div>
        {rows}
      </div>
    );
  }
}

export default ProductGrid;
