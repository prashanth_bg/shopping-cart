import React from 'react';

class ProductInfo extends React.Component {
  render () {
    return (
      <div className="product-info card text-center">
        <div className="brand">{this.props.brand}</div>
        <div className="title">{this.props.title}</div>
        <div className="price">${this.props.price}</div>
      </div>
    );
  }
}

export default ProductInfo;
