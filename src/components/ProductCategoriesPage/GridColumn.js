import React from 'react';
import ProductHover from './ProductHover';
import ProductInfo from './ProductInfo';
import {
  Column
} from 'react-foundation';

class GridColumn extends React.Component {
  render () {
    return (
      <Column small={this.props.small} large={this.props.large}>
        <div className="product-image text-center">
          <img src={this.props.image}></img>
          <div className="overlay" >
            <ProductHover id={this.props.id}></ProductHover>
          </div>
        </div>
        <ProductInfo brand={this.props.brand} title={this.props.title}
          price={this.props.price}>
        </ProductInfo>
      </Column>
    );
  }
}

export default GridColumn;
