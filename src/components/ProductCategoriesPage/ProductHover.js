import React from 'react';
import { withRouter } from 'react-router-dom';
import {
  Button
} from 'react-foundation';


class ProductHover extends React.Component {

  constructor(props) {
    super(props);
  }

  render () {

    const ViewDetailsButton = withRouter(({ history}) => (
      <Button onClick={() => { history.push('/details/' + this.props.id) }}>
        View Details
      </Button>
    ))

    return (
      <div className="product-hover card text-center">
        <div className="button-details">
          <ViewDetailsButton></ViewDetailsButton>
        </div>
        <div className="button-add-to-cart">
          <Button>Add to cart</Button>
        </div>
      </div>
    );
  }
}

export default ProductHover;
