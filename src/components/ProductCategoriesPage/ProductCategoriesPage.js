import React from 'react';
import Banner from './Banner';
import ProductGrid from './ProductGrid';
import productsData from '../../mock/data.json'

class ProductCategoriesPage extends React.Component {
  render () {
    return (
      <div>
        <div className="category-media-area">
          <Banner></Banner>
        </div>

        <div className="category-items-area">
          <ProductGrid products={productsData}></ProductGrid>
        </div>
      </div>
    );
  }
}

export default ProductCategoriesPage
