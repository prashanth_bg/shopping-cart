import React from 'react';
import {
  TopBar,
  TopBarTitle,
  TopBarLeft,
  TopBarRight,
  Menu,
  MenuItem
} from 'react-foundation';
import logo from '../images/logo.png';

class AppMenu extends React.Component {
  render () {
    return (
      <TopBar>
        <TopBarTitle>
          <img src={logo} className="category-top-bar-logo" alt="Logo" />
        </TopBarTitle>
        <TopBarLeft>
            <Menu>
              <MenuItem><a>HOME</a></MenuItem>
              <MenuItem><a>SHOP</a></MenuItem>
              <MenuItem><a>JOURNAL</a></MenuItem>
              <MenuItem><a>MORE</a></MenuItem>
            </Menu>
        </TopBarLeft>
        <TopBarRight>
          <Menu>
            <MenuItem><a>CART</a></MenuItem>
          </Menu>
        </TopBarRight>
      </TopBar>
    );
  }
}

export default AppMenu;
