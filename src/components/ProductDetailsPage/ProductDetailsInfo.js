import React from 'react';
import { Row, Column, Button } from 'react-foundation';

class ProductDetailsInfo extends React.Component {
  render () {
    return (
      <div className="info card">
      <Row className="display">
        <Column small={8} large={4}>
          <div className='details-image'>
            <img src={require('../../images/' + this.props.details.image)}></img>
          </div>
        </Column>
        <Column className='details-info' small={4} large={4}>
          <Row>
            <Column className="text-center">
              <div className="details-brand">
                {this.props.details.brand}
              </div>
            </Column>
          </Row>
          <Row>
            <Column className="text-center">
              <div className="details-title">
                {this.props.details.title}
              </div>
            </Column>
          </Row>
          <Row>
            <Column className="text-center">
              <div className="details-price">
                {'$' + this.props.details.price}
              </div>
            </Column>
          </Row>
          <Row>
            <Column className="text-center">
              <div className="details-description">
                {this.props.details.description}
              </div>
            </Column>
          </Row>
          <Row>
            <Column className="text-center">
              <Row className="details-controls">
                <Column small={2} large={4}>
                  <div className="incrementer">
                    <Button>+</Button>
                  </div>
                </Column>
                <Column small={2} large={6}>
                  <div className="add-to-cart">
                    <Button>Add to cart</Button>
                  </div>
                </Column>
              </Row>
            </Column>
          </Row>
        </Column>
      </Row>
      </div>
    );
  }
}

export default ProductDetailsInfo;
