import React from 'react';
import ProductDetailsPath from './ProductDetailsPath';
import ProductDetailsInfo from './ProductDetailsInfo';


class ProductDetailsPage extends React.Component {
  render () {
    return (
      <div className="product-details-view card">
        <ProductDetailsPath title={this.props.details.title}></ProductDetailsPath>
        <ProductDetailsInfo details={this.props.details}></ProductDetailsInfo>
      </div>
    );
  }
}

export default ProductDetailsPage;
