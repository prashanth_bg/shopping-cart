import React from 'react';
import { Row, Column } from 'react-foundation';
const pathPrefix = 'home / plates / ';

class ProductDetailsPath extends React.Component {
  render() {
    return (
      <Row className="details-row">
        <Column className="details-column">
          <div className="path-text text-center">
            { pathPrefix +  this.props.title}
          </div>
        </Column>
      </Row>
    );
  }
}

export default ProductDetailsPath
