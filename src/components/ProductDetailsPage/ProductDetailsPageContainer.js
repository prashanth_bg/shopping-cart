import React from 'react';
import ProductDetailsPage from './ProductDetailsPage';
import PRODUCTS from '../../mock/data.json';

class ProductDetailsPageContainer extends React.Component {
  render () {
    let productId = this.props.match.params.id;
    let details = PRODUCTS[parseInt(productId, 10)]
    return (
      <ProductDetailsPage details={details}></ProductDetailsPage>
    );
  }
}

export default ProductDetailsPageContainer;
