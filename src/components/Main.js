import React from 'react';
import AppMenu from './AppMenu';

class Main extends React.Component {

  render() {
    return (
      <div className="category">

        <div className="category-top-bar">
          <AppMenu></AppMenu>
        </div>

        {this.props.children}

      </div>
    );
  }
}

export default Main;
