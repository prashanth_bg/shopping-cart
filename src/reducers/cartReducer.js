const initialState = {
  productIdsInCart: [],
  quantityPerId: {},
  total: 0
}

remove = (arr, element) => {
  return arr.filter( param => param !== element);
}

const productIdsInCart = (state = initialState.productIdsInCart, action) => {
  switch(aciton.type) {
    case ADD_TO_CART:
      state.indexOf(action.id) !== -1 ? return state :
        return [...state, action.id];
    case REMOVE_FROM_CART:
      state.indexOf(action.id) !== -1 ? return remove(state, action.id) :
        return state;
    default:
     return state;
  }
}

const quantityPerId = (state = initialState.quantityPerId, action) => {
  switch(action.type) {
    case ADD_TO_CART:
      return {...state, [id]: state[id] ? state[id] + 1 : 0}
    case REMOVE_FROM_CART:
      return {...state, [id]: state[id] >0 state[id] - 1: 0}
    default:
      return state;
  }
}
